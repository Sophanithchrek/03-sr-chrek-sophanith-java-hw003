public class Volunteer extends StaffMember {

    // Constructor
    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    // Override method of parent-class to show info
    public String toString() {
        return "ID : "+getStaffId()+
                "\nName : "+getStaffName()+
                "\nAddress : "+getStaffAddress()+
                "\nThank!";
    }

    // Implement method of parent-class
    public double pay() {
        // Volunteer employee get some allowance
        return 0;
    }

    /*
        - Override method of Interface: Comparable
        - purpose to sort data in Collection: arraylist of abstract class: StaffMember
     */
    @Override
    public int compareTo(StaffMember staffObj) {
        return getStaffName().compareTo(staffObj.getStaffName());
    }
}
