import java.util.Collection;
import java.util.Comparator;
import java.util.Scanner;

abstract class StaffMember implements Comparable<StaffMember> {

    // Instance variable
    protected int staffId;
    protected String staffName;
    protected String staffAddress;

    // Constructor
    public StaffMember(int staffId, String staffName, String staffAddress) {
        this.staffId = staffId;
        this.staffName = staffName;
        this.staffAddress = staffAddress;
    }

    // Override build-in method: toString()
    @Override
    public String toString() {
        return "ID : "+getStaffId()+
                "\nName : "+getStaffName()+
                "\nAddress : "+getStaffAddress();
    }

    // Abstract method
    public abstract double pay();

    // Getter and Setter
    public int getStaffId() {
        return staffId;
    }

    public void setStaffId(int staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffAddress() {
        return staffAddress;
    }

    public void setStaffAddress(String staffAddress) {
        this.staffAddress = staffAddress;
    }
}
