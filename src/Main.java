import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Main {

    // === Note: Declare access modifier as private purpose to non-access from other class === //
    private static final Scanner scanner = new Scanner(System.in);

    // Initialize final arraylist to store data of all volunteer staff
    private static final ArrayList<StaffMember> allStaffData = new ArrayList<>();

    public static void main(String[] args) {

        // Initialize data from sub-class: Volunteer class
        Volunteer staffVolunteer = new Volunteer(1, "Dara".toUpperCase(), "Phnom Penh");

        // Initialize data from sub-class: Salaried class
        SalariedStaff staffSalaried = new SalariedStaff(2, "Seyha".toUpperCase(), "Phnom Penh", 350, 50);

        // Initialize data from sub-class: Salaried class
        HourlyStaff staffHourly = new HourlyStaff(3, "Linda".toUpperCase(), "Kandal", 70, 1.5);

        // Add all staff data to List
        allStaffData.add(staffVolunteer);
        allStaffData.add(staffSalaried);
        allStaffData.add(staffHourly);

        // After add some, call method to display all data
        displayAllStaffData();

        MENU_LABEL:
        while (true) {
            // Show option for user to choose
            System.out.println("===== Staff Management =====");
            System.out.println("1). Add Staff  2). Edit  3). Remove  4). Exit\n");

            boolean isTypeNumberWithValidOption = false;
            String userInputData;
            do {
                System.out.print("=> Choose option(1-4) : ");
                userInputData = scanner.nextLine();

                if (!userInputData.equals("")) {
                    try {
                        // Convert value of userInputData (String) to Integer
                        int optionValue = Integer.parseInt(userInputData);

                        if (optionValue > 0 && optionValue <= 4) {
                            isTypeNumberWithValidOption = true;

                            // Option 1: Add new staff
                            if (optionValue == 1) {
                                System.out.println("---------------------------------");
                                System.out.println("1). Volunteer  2). Hourly Staff  3). Salaried Staff  4). Back\n");

                                // Validation on sub-option
                                boolean isValidTypeWithValidSubOption = false;
                                String userInputSubOptionData;
                                do {
                                    System.out.print("=> Choose sub-option(1-4) : ");
                                    userInputSubOptionData = scanner.nextLine();

                                    if (!userInputSubOptionData.equals("")) {
                                        try {
                                            // Convert value of userInputSubOptionData (String) to Integer
                                            int subOptionValue = Integer.parseInt(userInputSubOptionData);

                                            if (subOptionValue > 0 && subOptionValue <= 4) {
                                                isValidTypeWithValidSubOption = true;

                                                // Sub-option 1: Add volunteer staff
                                                if (subOptionValue == 1) {
                                                    addVolunteerStaffData();
                                                    continue MENU_LABEL;
                                                }
                                                // Sub-option 2: Add hourly staff
                                                else if (subOptionValue == 2) {
                                                    addHourlyStaffData();
                                                    continue MENU_LABEL;
                                                }
                                                // Sub-option 3: Add salaried staff
                                                else if (subOptionValue == 3) {
                                                    addSalariedStaffData();
                                                    continue MENU_LABEL;
                                                }
                                                // Sub-option 4: Go back
                                                else {
                                                    System.out.println();
                                                    continue MENU_LABEL;
                                                }

                                            }
                                            else {
                                                System.out.println("WARNING: PLEASE CHOOSE VALID OPTION!");
                                            }

                                        } catch (NumberFormatException ex) {
                                            System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                                        }
                                    }

                                } while (userInputSubOptionData.equals("") || !isValidTypeWithValidSubOption);

                            }
                            // Option 2: Update staff's info
                            else if (optionValue == 2) {
                                // Call method to edit data in arraylist:
                                editStaffData();
                            }
                            // Option 3: Remove all staff's info
                            else if (optionValue == 3) {
                                removeStaffData();
                            }
                            // Option 4: Exit program
                            else {
                                System.out.println("--- GOOD BYE ---");
                                System.exit(0);
                            }

                        } else {
                            System.out.println("WARNING: PLEASE CHOOSE VALID OPTION!");
                        }
                    } catch (NumberFormatException ex) {
                        System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                    }
                }
            } while (userInputData.equals("") || !isTypeNumberWithValidOption);
        }

    }

    // ----- Method ----- //
    private static void displayAllStaffData() {
        // Sort data of arraylist: allStaffData using staffName
        Collections.sort(allStaffData);

        // Display all staff member's data
        System.out.println("\n===== Display Staff Data =====");
        for (StaffMember staff : allStaffData) {
            System.out.println(staff);
            System.out.println("---------------------------------");
        }
        System.out.println();
    }

    private static void addVolunteerStaffData() {
        System.out.println("===== Insert Staff Data =====");

        // Get data from method with user can input using console
        int staffId = inputStaffID();
        String staffName = inputStaffName();
        String staffAddress = inputStaffAddress();

        // Construct new data and add to arraylist: allStaffData
        Volunteer staffVolunteer = new Volunteer(staffId, staffName.toUpperCase(), staffAddress);
        allStaffData.add(staffVolunteer);

        // Display data that already have some data
        displayAllStaffData();
    }

    private static void addHourlyStaffData() {
        System.out.println("===== Insert Staff Data =====");

        // Get data from method with user can input using console
        int staffId = inputStaffID();
        String staffName = inputStaffName();
        String staffAddress = inputStaffAddress();
        int hourlyWork = inputHourlyWorkData();
        double rate = inputRateData();

        // Construct new hourly staff data and add to arraylist: allStaffData
        HourlyStaff staffHourly = new HourlyStaff(staffId, staffName.toUpperCase(), staffAddress, hourlyWork, rate);
        allStaffData.add(staffHourly);

        // Display data that already have some initialize data
        displayAllStaffData();
    }

    private static void addSalariedStaffData() {
        System.out.println("===== Insert Staff Data =====");

        // Get data from method with user can input using console
        int staffId = inputStaffID();
        String staffName = inputStaffName();
        String staffAddress = inputStaffAddress();
        double salary = inputStaffSalary();
        double bonusSalary = inputStaffBonusSalary();

        // Construct new salaried staff data and add to arraylist: allStaffData
        SalariedStaff staffSalaried = new SalariedStaff(staffId, staffName.toUpperCase(), staffAddress, salary, bonusSalary);
        allStaffData.add(staffSalaried);

        // Display data that already have some initialize data
        displayAllStaffData();
    }

    private static void editStaffData() {
        System.out.println("===== Edit Staff Data =====");

        boolean isTypeNumber = false;
        String idForUpdateAsString;
        do {
            System.out.print("=> Enter Staff ID to Update : ");
            idForUpdateAsString = scanner.nextLine();

            if (!idForUpdateAsString.equals("")) {
                try {
                    // Convert from indexForUpdateAsString to Integer
                    int idForUpdate = Integer.parseInt(idForUpdateAsString);
                    isTypeNumber = true;

                    boolean isFoundId = false;
                    for (int ind = 0; ind < allStaffData.size(); ind++) {
                        if (allStaffData.get(ind).getStaffId() == idForUpdate) {

                            // Show Data before update to new data
                            System.out.println("\n"+allStaffData.get(ind).toString()+"\n");

                            // Check class before update
                            switch (allStaffData.get(ind).getClass().getName()) {
                                case "Volunteer": {
                                    // Get data from method with user can input using console
                                    String staffNameForUpdate = inputStaffName();
                                    String staffAddressForUpdate = inputStaffAddress();

                                    // Update new data in arraylist: allStaffData
                                    allStaffData.set(ind, new Volunteer(idForUpdate, staffNameForUpdate, staffAddressForUpdate));

                                    // break out from switch
                                    break;
                                }
                                case "HourlyStaff": {
                                    // Get data from method with user can input using console
                                    String staffNameForUpdate = inputStaffName();
                                    String staffAddressForUpdate = inputStaffAddress();
                                    int hourlyWork = inputHourlyWorkData();
                                    double rate = inputRateData();

                                    // Update new data in arraylist: allStaffData
                                    allStaffData.set(ind, new HourlyStaff(idForUpdate, staffNameForUpdate, staffAddressForUpdate, hourlyWork, rate));

                                    // break out from switch
                                    break;
                                }
                                case "SalariedStaff":
                                    // Get data from method with user can input using console
                                    String staffNameForUpdate = inputStaffName();
                                    String staffAddressForUpdate = inputStaffAddress();
                                    double salary = inputStaffSalary();
                                    double bonus = inputStaffBonusSalary();

                                    // Update new data in arraylist: allStaffData
                                    allStaffData.set(ind, new SalariedStaff(idForUpdate, staffNameForUpdate, staffAddressForUpdate, salary, bonus));

                                    // break out from switch
                                    break;
                            }

                            // Show whole data with new data
                            displayAllStaffData();

                            isFoundId = true;
                            break;
                        }
                    }

                    if (!isFoundId) {
                        System.out.println("WARNING: NOT FOUND ID FOR UPDATE!\n");
                    }

                } catch (NumberFormatException ex) {
                    System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                }
            }

        } while (idForUpdateAsString.equals("") || !isTypeNumber);

        Scanner pause = new Scanner(System.in);
        System.out.print("\nPress \"ENTER\" to continue...");
        pause.nextLine();
    }

    private static void removeStaffData() {
        System.out.println("===== Remove Staff Data =====");

        boolean isTypeNumber = false;
        String idForRemoveDataAsString;
        do {
            System.out.print("=> Enter Staff ID to Remove : ");
            idForRemoveDataAsString = scanner.nextLine();

            if (!idForRemoveDataAsString.equals("")) {
                try {
                    // Convert data from "idForRemoveAsString" to Integer
                    int idForRemoveData = Integer.parseInt(idForRemoveDataAsString);
                    isTypeNumber = true;

                    boolean isFoundIdForRemove = false;
                    for (int ind = 0; ind < allStaffData.size(); ind++) {
                        if (allStaffData.get(ind).getStaffId() == idForRemoveData) {
                            // Process to remove element from arraylist: allStaffData
                            allStaffData.remove(ind);

                            // Show whole data with new data
                            displayAllStaffData();

                            isFoundIdForRemove = true;

                            // break to reduce process of loop to find id
                            break;
                        }
                    }

                    if (!isFoundIdForRemove) {
                        System.out.println("WARNING: NOT FOUND ID FOR REMOVE!\n");
                    }

                } catch (NumberFormatException ex) {
                    System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                }
            }

        } while (idForRemoveDataAsString.equals("") || !isTypeNumber);

    }

    // --- Optional Method to reduce line of code --- //
    private static int inputStaffID() {
        boolean isTypeNumberWithValidateOnStaffId = false;
        String staffIdAsString;
        int staffId = 0;
        do {
            System.out.print("=> Enter Staff Member's ID        : ");
            staffIdAsString = scanner.nextLine();

            if (!staffIdAsString.equals("")) {
                try {
                    // Convert from data from "staffIdAsString" to Integer
                    staffId = Integer.parseInt(staffIdAsString);
                    if (staffId < 0)
                        System.out.println("WARNING: ACCEPT ONLY POSITIVE NUMBER!");
                    else {
                        boolean isFoundDuplicateStaffId = false;
                        for (StaffMember staffData : allStaffData) {
                            // Validate if user input id that already exist in arraylist: allStaffData
                            if (staffData.getStaffId() == staffId) {
                                System.out.println("WARNING: DUPLICATE ID, PLEASE INPUT AGAIN!");

                                // If found id that duplicate, set found = true
                                isFoundDuplicateStaffId = true;
                                break;
                            }
                        }

                        // If staff id not found duplicate, user can take this id to insert
                        if (!isFoundDuplicateStaffId)
                            isTypeNumberWithValidateOnStaffId = true;
                    }
                } catch (NumberFormatException ex) {
                    System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                }
            }
        } while (staffIdAsString.equals("") || !isTypeNumberWithValidateOnStaffId);

        // After validation, need return value
        return staffId;
    }

    private static String inputStaffName() {
        String staffName;
        do {
            System.out.print("=> Enter Staff Member's Name      : ");
            staffName = scanner.nextLine();
        } while (staffName.equals(""));

        return staffName;
    }

    private static String inputStaffAddress() {
        String staffAddress;
        do {
            System.out.print("=> Enter Staff Member's Address    : ");
            staffAddress = scanner.nextLine();
        } while (staffAddress.equals(""));

        return staffAddress;
    }

    private static int inputHourlyWorkData() {
        /*
            - Validation on minus(-) value
            - Validation of type that user input
         */
        boolean isTypeNumberWithPositiveValue = false;
        String hourlyWorkDataAsString;
        int hourlyWork = 0;
        do {
            System.out.print("=> Enter Staff's Hourly Work      : ");
            hourlyWorkDataAsString = scanner.nextLine();
            if (!hourlyWorkDataAsString.equals("")) {
                try {
                    // Convert data from "hourlyWorkDataAsString" to Integer
                    hourlyWork = Integer.parseInt(hourlyWorkDataAsString);

                    if (hourlyWork < 0)
                        System.out.println("WARNING: ACCEPT ONLY POSITIVE NUMBER!");
                    else
                        isTypeNumberWithPositiveValue = true;

                } catch (NumberFormatException ex) {
                    System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                }
            }
        } while (hourlyWorkDataAsString.equals("") || !isTypeNumberWithPositiveValue);

        // After validation data, need return value
        return hourlyWork;
    }

    private static double inputRateData() {
        /*
            - Validation on minus(-) value
            - Validation of type that user input
         */
        boolean isTypeNumberWithPositiveValue = false;
        String rateDataAsString;
        double rateData = 0;
        do {
            System.out.print("=> Enter Staff's Hourly Work      : ");
            rateDataAsString = scanner.nextLine();
            if (!rateDataAsString.equals("")) {
                try {
                    // Convert data from "rateDataAsString" to Integer
                    rateData = Double.parseDouble(rateDataAsString);

                    if (rateData < 0)
                        System.out.println("WARNING: ACCEPT ONLY POSITIVE NUMBER!");
                    else
                        isTypeNumberWithPositiveValue = true;

                } catch (NumberFormatException ex) {
                    System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                }
            }
        } while (rateDataAsString.equals("") || !isTypeNumberWithPositiveValue);

        // After validation data, need return value
        return rateData;
    }

    private static double inputStaffSalary() {
        /*
            - Validation on minus(-) value
            - Validation of type that user input
         */
        boolean isTypeNumberForSalary = false;
        String salaryAsString;
        double salary = 0;
        do {
            System.out.print("=> Enter Staff Member's Salary    : ");
            salaryAsString = scanner.nextLine();

            if (!salaryAsString.equals("")) {
                try {
                    // Convert from data from "salaryAsString" to Integer
                    salary = Integer.parseInt(salaryAsString);
                    if (salary < 0)
                        System.out.println("WARNING: ACCEPT ONLY POSITIVE NUMBER!");
                    else
                        isTypeNumberForSalary = true;
                } catch (NumberFormatException ex) {
                    System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                }
            }
        } while (salaryAsString.equals("") || !isTypeNumberForSalary);

        // After validation data, need return value
        return salary;
    }

    private static double inputStaffBonusSalary() {
        /*
            - Validation on minus(-) value
            - Validation of type that user input
         */
        boolean isTypeNumberForBonusSalary = false;
        String bonusSalaryAsString;
        double bonusSalary = 0;
        do {
            System.out.print("=> Enter Staff Member's Bonus    : ");
            bonusSalaryAsString = scanner.nextLine();

            if (!bonusSalaryAsString.equals("")) {
                try {
                    // Convert from data from "bonusAsString" to Integer
                    bonusSalary = Double.parseDouble(bonusSalaryAsString);
                    if (bonusSalary < 0)
                        System.out.println("WARNING: ACCEPT ONLY POSITIVE NUMBER!");
                    else
                        isTypeNumberForBonusSalary = true;
                } catch (NumberFormatException ex) {
                    System.out.println("WARNING: ACCEPT ONLY NUMBER!");
                }
            }
        } while (bonusSalaryAsString.equals("") || !isTypeNumberForBonusSalary);

        // After validation data, need return value
        return bonusSalary;
    }

}