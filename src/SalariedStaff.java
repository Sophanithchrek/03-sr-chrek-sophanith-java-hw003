public class SalariedStaff extends StaffMember {

    // Instance variable
    private double annualSalary;
    private double bonusSalary;

    // Constructor
    public SalariedStaff(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.annualSalary = salary;
        this.bonusSalary = bonus;
    }

    // Override method of parent-class to show info
    public String toString() {
        return "ID : "+getStaffId()+
                "\nName : "+getStaffName()+
                "\nAddress : "+getStaffAddress()+
                "\nSalary : "+getAnnualSalary()+
                "\nBonus : "+getBonusSalary()+
                "\nPayment : "+pay()+" USD";
    }

    @Override
    public double pay() {
        return getAnnualSalary() + getBonusSalary();
    }

    // Getter and Setter
    public double getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(double annualSalary) {
        this.annualSalary = annualSalary;
    }

    public double getBonusSalary() {
        return bonusSalary;
    }

    public void setBonusSalary(double bonusSalary) {
        this.bonusSalary = bonusSalary;
    }

    /*
        - Override method of Interface: Comparable
        - purpose to sort data in Collection: arraylist of abstract class: StaffMember
     */
    @Override
    public int compareTo(StaffMember staffObj) {
        return getStaffName().compareTo(staffObj.getStaffName());
    }

}
