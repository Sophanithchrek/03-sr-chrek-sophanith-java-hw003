public class HourlyStaff extends StaffMember {
    // Instance variable
    private int hoursWork;
    private double rate;

    // Constructor
    public HourlyStaff(int id, String name, String address, int hour, double rate) {
        super(id, name, address);
        this.hoursWork = hour;
        this.rate = rate;
    }

    // Override method of parent-class to show info
    public String toString() {
        return "ID : "+getStaffId()+
                "\nName : "+getStaffName()+
                "\nAddress : "+getStaffAddress()+
                "\nHour Work : "+getHoursWork()+
                "\nRate : "+getRate()+
                "\nPayment : "+pay()+" USD";
    }

    @Override
    public double pay() {
        // ---- Calculate hoursWork with rate
        return getHoursWork() * getRate();
    }

    // Getter and Setter
    public int getHoursWork() {
        return hoursWork;
    }

    public void setHoursWork(int hoursWork) {
        this.hoursWork = hoursWork;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    /*
        - Override method of Interface: Comparable
        - purpose to sort data in Collection: arraylist of abstract class: StaffMember
     */
    @Override
    public int compareTo(StaffMember staffObj) {
        return getStaffName().compareTo(staffObj.getStaffName());
    }

}
